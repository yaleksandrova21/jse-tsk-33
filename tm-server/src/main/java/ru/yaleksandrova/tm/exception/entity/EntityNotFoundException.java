package ru.yaleksandrova.tm.exception.entity;

import ru.yaleksandrova.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error! Entity not found");
    }

}
