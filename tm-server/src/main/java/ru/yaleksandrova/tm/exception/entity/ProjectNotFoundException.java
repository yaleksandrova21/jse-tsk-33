package ru.yaleksandrova.tm.exception.entity;

import ru.yaleksandrova.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found");
    }

}
