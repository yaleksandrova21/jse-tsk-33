package ru.yaleksandrova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.repository.IOwnerRepository;
import ru.yaleksandrova.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.remove(entity);
    }

    @Override
    public void clear(@NotNull final String userId) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.clear();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(o -> userId.equals(o.getUserId()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        for(E entity:list){
            if(id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final List<E> list = findAll(userId);
        return list.get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        final Optional<E> ownerEntities =  Optional.ofNullable(findById(userId, id));
        ownerEntities.ifPresent(this::remove);
        return ownerEntities.orElse(null);
    }

    @NotNull
    public Integer size(@NotNull final String userId) {
        final List<E> list = findAll(userId);
        return list.size();
    }

}


