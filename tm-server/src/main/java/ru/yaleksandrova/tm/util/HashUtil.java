package ru.yaleksandrova.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.api.ISaltSetting;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    static String salt(
            @NotNull final String value, 
            @NotNull final Integer iteration, 
            @NotNull final String secret
    ) {
        if (value == null) return null;
        @NotNull String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    static String salt(@NotNull final ISaltSetting setting, @NotNull final String value) {
        @NotNull final String secret = setting.getPasswordSecret();
        @NotNull final Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }

    @NotNull
    static String md5(final String value) {
        if (value == null) return null;
        try {
            @NotNull java.security.MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i)
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
