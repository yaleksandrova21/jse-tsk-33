package ru.yaleksandrova.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface ApplicationUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

}
