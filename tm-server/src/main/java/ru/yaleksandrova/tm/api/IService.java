package ru.yaleksandrova.tm.api;

import ru.yaleksandrova.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E>  {

}
