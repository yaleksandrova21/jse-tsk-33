package ru.yaleksandrova.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.IService;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.model.Session;
import ru.yaleksandrova.tm.model.User;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    @Nullable
    Session sign(@Nullable Session session);

    boolean isValid(@Nullable Session session);

    void validate(@Nullable Session session);

    void validate(@Nullable Session session, @Nullable Role role);

    @NotNull
    User getUser(@Nullable Session session);

    @NotNull
    String getUserId(@Nullable Session session);

    @NotNull
    List<Session> getListSession(@Nullable Session session);

    void close(@Nullable Session session);

    void closeAll(@NotNull List<Session> sessionList);

}

