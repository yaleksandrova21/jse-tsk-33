package ru.yaleksandrova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project startByIndex(String userId, @NotNull Integer index);

    @NotNull
    Project startByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project startById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project finishById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project finishByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project finishByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Project changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    Project changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

}
