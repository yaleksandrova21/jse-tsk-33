package ru.yaleksandrova.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.IEndpoint;
import ru.yaleksandrova.tm.model.Session;
import ru.yaleksandrova.tm.model.User;

public interface IAdminUserEndpoint extends IEndpoint<User> {

    @Nullable
    User findByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User removeByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User lockByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User unlockByLogin(@Nullable Session session, @Nullable String login);

}

