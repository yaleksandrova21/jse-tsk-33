package ru.yaleksandrova.tm.api.controller;

import ru.yaleksandrova.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void clearTask();

    void showTask(Task task);

    void createTask();

    void showById();

    void showByIndex();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

    void findAllTaskByProjectId();

    void bindTaskToProjectById();

    void unbindTaskById();

}
