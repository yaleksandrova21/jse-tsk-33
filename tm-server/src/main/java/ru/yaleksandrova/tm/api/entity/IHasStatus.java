package ru.yaleksandrova.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull Status getStatus();

    void setStatus(@NotNull Status status);

}
