package ru.yaleksandrova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.IEndpoint;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Session;

import java.util.List;

public interface IProjectEndpoint extends IEndpoint<Project> {

    void clearProjects(@Nullable Session session);

    @NotNull
    List<Project> findAllProjects(@Nullable Session session);

    @NotNull
    Project createProject(@Nullable Session session, @Nullable String name);

    @NotNull
    Project createProjectWithDescription(
            @Nullable Session session,
            @Nullable String name,
            @Nullable String description);

    @Nullable
    Project findProjectById(@Nullable Session session, @Nullable String id);

    @Nullable
    Project findProjectByIndex(@Nullable Session session, @Nullable Integer index);

    @Nullable
    Project findProjectByName(@Nullable Session session, @Nullable String name);

    @Nullable
    Project removeProjectById(@Nullable Session session, @Nullable String id);

    @Nullable
    Project removeProjectByIndex(@Nullable Session session, @Nullable Integer index);

    @Nullable
    Project removeProjectByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Project updateProjectById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateProjectByIndex(
            @Nullable Session session,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project startProjectById(@Nullable Session session, @Nullable String id);

    @NotNull
    Project startProjectByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Project startProjectByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Project finishProjectById(@Nullable Session session, @Nullable String id);

    @NotNull
    Project finishProjectByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Project finishProjectByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Project changeProjectStatusById(@Nullable Session session, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable Session session, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByName(@Nullable Session session, @Nullable String name, @Nullable Status status);

}

