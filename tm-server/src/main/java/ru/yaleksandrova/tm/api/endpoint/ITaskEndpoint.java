package ru.yaleksandrova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.IEndpoint;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Session;
import ru.yaleksandrova.tm.model.Task;

import java.util.List;

public interface ITaskEndpoint extends IEndpoint<Task> {

    void clearTasks(@Nullable Session session);

    @NotNull
    List<Task> findAllTasks(@Nullable Session session);

    @NotNull
    Task createTask(@Nullable Session session, @Nullable String name);

    @NotNull
    Task createTaskWithDescription(@Nullable Session session, @Nullable String name, @Nullable String description);

    @Nullable
    Task findTaskById(@Nullable Session session, @Nullable String id);

    @Nullable
    Task findTaskByIndex(@Nullable Session session, @Nullable Integer index);

    @Nullable
    Task findTaskByName(@Nullable Session session, @Nullable String name);

    @Nullable
    Task removeTaskById(@Nullable Session session, @Nullable String id);

    @Nullable
    Task removeTaskByIndex(@Nullable Session session, @Nullable Integer index);

    @Nullable
    Task removeTaskByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Task updateTaskById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateTaskByIndex(
            @Nullable Session session,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task startTaskById(@Nullable Session session, @Nullable String id);

    @NotNull
    Task startTaskByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Task startTaskByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Task finishTaskById(@Nullable Session session, @Nullable String id);

    @NotNull
    Task finishTaskByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Task finishTaskByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Task changeTaskStatusById(@Nullable Session session, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable Session session, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByName(@Nullable Session session, @Nullable String name, @Nullable Status status);


    @NotNull
    Task bindTaskByProjectId(@Nullable Session session, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskByProjectId(@Nullable Session session, @Nullable String projectId, @Nullable String taskId);

}

