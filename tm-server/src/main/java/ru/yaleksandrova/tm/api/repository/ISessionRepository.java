package ru.yaleksandrova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

}

