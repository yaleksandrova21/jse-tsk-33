package ru.yaleksandrova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.IEndpoint;
import ru.yaleksandrova.tm.model.Session;
import ru.yaleksandrova.tm.model.User;

public interface ISessionEndpoint extends IEndpoint<Session> {

    @Nullable
    Session openSession(@Nullable String login, @Nullable String password);

    @NotNull
    User getUser(@Nullable Session session);

    void closeSession(@Nullable Session session);

}

