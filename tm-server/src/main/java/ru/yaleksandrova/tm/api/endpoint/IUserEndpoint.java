package ru.yaleksandrova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.IEndpoint;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.model.Session;
import ru.yaleksandrova.tm.model.User;

import java.util.List;

public interface IUserEndpoint extends IEndpoint<User> {

    boolean checkRoles(@Nullable Session session, @Nullable List<Role> roles);

    @NotNull
    User createUser(@Nullable String login, @Nullable String password);

    @NotNull
    User createUserWithEmail(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User createUserWithRole(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User setPassword(@Nullable Session session, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable Session session,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

}
