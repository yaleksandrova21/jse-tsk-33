package ru.yaleksandrova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.endpoint.IAdminEndpoint;
import ru.yaleksandrova.tm.api.sevice.IServiceLocator;
import ru.yaleksandrova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().saveBackup();
    }

    @Override
    @WebMethod
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().loadBackup();
    }

    @Override
    @WebMethod
    public void saveBinaryData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().saveBinaryData();
    }

    @Override
    @WebMethod
    public void loadBinaryData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().loadBinaryData();
    }

    @Override
    @WebMethod
    public void saveBase64Data(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().saveBase64Data();
    }

    @Override
    @WebMethod
    public void loadBase64Data(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().loadBase64Data();
    }

    @Override
    @WebMethod
    public void saveFasterXmlData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "isJson", partName = "isJson") final boolean isJson
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().saveFasterXmlData(isJson);
    }

    @Override
    @WebMethod
    public void loadFasterXmlData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "isJson", partName = "isJson") final boolean isJson
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().loadFasterXmlData(isJson);
    }

    @Override
    @WebMethod
    public void saveJaxbData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "isJson", partName = "isJson") final boolean isJson
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().saveJaxbData(isJson);
    }

    @Override
    @WebMethod
    public void loadJaxbData(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "isJson", partName = "isJson") final boolean isJson
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getDataService().loadJaxbData(isJson);
    }

}
