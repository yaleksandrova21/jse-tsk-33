package ru.yaleksandrova.tm.endpoint;

import ru.yaleksandrova.tm.api.sevice.IServiceLocator;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractEndpoint {

    @NotNull
    protected IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

