package ru.yaleksandrova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.endpoint.IAdminUserEndpoint;
import ru.yaleksandrova.tm.api.sevice.IServiceLocator;
import ru.yaleksandrova.tm.model.Session;
import ru.yaleksandrova.tm.model.User;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Optional;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public User findByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final Optional<User> user = Optional.of(serviceLocator.getUserService().findByLogin(login));
        return user.orElse(null);
    }

    @Nullable
    @Override
    public User removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final Optional<User> user = Optional.of(serviceLocator.getUserService().removeByLogin(login));
        return user.orElse(null);
    }

    @Nullable
    @Override
    public User lockByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final Optional<User> user = Optional.of(serviceLocator.getUserService().lockUserByLogin(login));
        return user.orElse(null);
    }

    @Nullable
    @Override
    public User unlockByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final Optional<User> user = Optional.of(serviceLocator.getUserService().unlockUserByLogin(login));
        return user.orElse(null);
    }

}
