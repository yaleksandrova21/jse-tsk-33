package ru.yaleksandrova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.endpoint.IProjectEndpoint;
import ru.yaleksandrova.tm.api.sevice.IServiceLocator;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear();
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().findAll(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Project createProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().create(userId, name);
    }

    @NotNull
    @Override
    @WebMethod
    public Project createProjectWithDescription(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().create(userId, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Project> project = Optional.of(serviceLocator.getProjectService().findById(userId, id));
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Project> project = Optional.of(serviceLocator.getProjectService().findByIndex(userId, index));
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Project> project = Optional.of(serviceLocator.getProjectService().findByName(userId, name));
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Project> project = Optional.of(serviceLocator.getProjectService().removeById(userId, id));
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Project> project = Optional.of(serviceLocator.getProjectService().removeByIndex(userId, index));
        return project.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Project> project = Optional.of(serviceLocator.getProjectService().removeByName(userId, name));
        return project.orElse(null);
    }

    @NotNull
    @Override
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().updateById(userId, id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().updateByIndex(userId, index, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Project startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().startById(userId, id);
    }

    @NotNull
    @Override
    @WebMethod
    public Project startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().startByIndex(userId, index);
    }

    @NotNull
    @Override
    @WebMethod
    public Project startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().startByName(userId, name);
    }

    @NotNull
    @Override
    @WebMethod
    public Project finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().finishById(userId, id);
    }

    @NotNull
    @Override
    @WebMethod
    public Project finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().finishByIndex(userId, index);
    }

    @NotNull
    @Override
    @WebMethod
    public Project finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().finishByName(userId, name);
    }

    @NotNull
    @Override
    @WebMethod
    public Project changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().changeStatusById(userId, id, status);
    }

    @NotNull
    @Override
    @WebMethod
    public Project changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    @WebMethod
    public Project changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().changeStatusByName(userId, name, status);
    }

}

