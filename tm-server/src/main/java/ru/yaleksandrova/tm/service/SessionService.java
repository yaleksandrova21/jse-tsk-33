package ru.yaleksandrova.tm.service;

import com.ctc.wstx.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.ISaltSetting;
import ru.yaleksandrova.tm.api.repository.ISessionRepository;
import ru.yaleksandrova.tm.api.sevice.IPropertyService;
import ru.yaleksandrova.tm.api.sevice.ISessionService;
import ru.yaleksandrova.tm.api.sevice.IUserService;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.empty.EmptyLoginException;
import ru.yaleksandrova.tm.exception.empty.EmptyPasswordException;
import ru.yaleksandrova.tm.exception.system.AccessDeniedException;
import ru.yaleksandrova.tm.model.Session;
import ru.yaleksandrova.tm.model.User;
import ru.yaleksandrova.tm.util.HashUtil;
import ru.yaleksandrova.tm.util.SignatureUtil;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public void clear() {
        sessionRepository.clear();
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login.isEmpty()) return false;
        if (password.isEmpty()) return false;
        @NotNull final Optional<User> user = Optional.of(userService.findByLogin(login));
        if (!user.isPresent()) return false;
        @Nullable String passwordHash = HashUtil.salt((ISaltSetting) propertyService, password);
        if (passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.get().getPasswordHash());
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session open(@Nullable final String login, @Nullable final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (!checkDataAccess(login, password)) throw new AccessDeniedException();
        @NotNull final Optional<User> user = Optional.of(userService.findByLogin(login));
        if (!user.isPresent()) throw new AccessDeniedException();
        @NotNull final Session session = new Session();
        session.setUserId(user.get().getId());
        session.setTimestamp(System.currentTimeMillis());
        sign(session);
        sessionRepository.add(session);
        return session;
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String secret = propertyService.getSessionSecret();
        @NotNull final Integer iteration = propertyService.getSessionIteration();
        @Nullable final String signature = SignatureUtil.sign(session, secret, iteration);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session tempSession = session.clone();
        if (tempSession == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        if (signatureSource.isEmpty()) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sign(tempSession).getSignature();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        @NotNull Optional<Session> sessionInRepo = Optional.of(sessionRepository.findById(session.getId()));
        if (!sessionInRepo.isPresent()) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final Optional<User> user = Optional.of(userService.findById(userId));
        if (!user.isPresent()) throw new AccessDeniedException();
        if (!role.equals(user.get().getRole())) throw new AccessDeniedException();
    }

    @NotNull
    @Override
    @SneakyThrows
    public User getUser(@Nullable Session session) {
        @NotNull final String userId = getUserId(session);
        @NotNull final Optional<User> user = Optional.of(userService.findById(userId));
        if (!user.isPresent()) throw new AccessDeniedException();
        return user.get();
    }

    @NotNull
    @Override
    public String getUserId(@Nullable final Session session) {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<Session> getListSession(@Nullable final Session session) {
        validate(session);
        return Collections.singletonList(sessionRepository.findById(session.getUserId()));
    }

    @Override
    public void close(@Nullable Session session) {
        validate(session);
        remove(session);
    }

    @Override
    public void closeAll(@NotNull final List<Session> sessionList) {
        for (@Nullable final Session session : sessionList) {
            try {
                validate(session);
                sessionRepository.remove(session);
            } catch (Exception e) {
                continue;
            }
        }
    }

}
