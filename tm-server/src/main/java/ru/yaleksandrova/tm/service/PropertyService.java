package ru.yaleksandrova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.sevice.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "1.0.0";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "default";

    @NotNull
    private static final String APPLICATION_DEVELOPER_KEY = "developer.name";

    @NotNull
    private static final String APPLICATION_DEVELOPER_DEFAULT = "default";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "default";

    @NotNull
    private static final String SERVER_HOST = "server.host";

    @NotNull
    private static final String DEFAULT_SERVER_HOST = "localhost";

    @NotNull
    private static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String DEFAULT_SERVER_PORT = "8080";

    @NotNull
    private static final String SESSION_SECRET_KEY = "SESSION_SECRET";

    @NotNull
    private static final String SESSION_SECRET_DEFAULT = "";

    @NotNull
    private static final String SESSION_ITERATION_KEY = "SESSION_ITERATION";

    @NotNull
    private static final String SESSION_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "backup.interval";

    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "30";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @Override
    public @NotNull String getPasswordSecret() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_SECRET_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(PASSWORD_SECRET_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public @NotNull Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(PASSWORD_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @Override
    public @NotNull String getApplicationVersion() {
        @Nullable final String systemProperty = System.getProperty(APPLICATION_VERSION_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(APPLICATION_VERSION_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @Override
    public @NotNull String getDeveloperName() {
        @Nullable final String systemProperty = System.getProperty(APPLICATION_DEVELOPER_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(APPLICATION_DEVELOPER_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(APPLICATION_DEVELOPER_KEY, APPLICATION_DEVELOPER_DEFAULT);
    }

    @Override
    public @NotNull String getDeveloperEmail() {
        @Nullable final String systemProperty = System.getProperty(DEVELOPER_EMAIL_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(DEVELOPER_EMAIL_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
    }

    @Override
    public @NotNull String getServerHost() {
        if (System.getProperties().containsKey(SERVER_HOST))
            return System.getProperty(SERVER_HOST);
        if (System.getenv().containsKey(SERVER_HOST))
            return System.getenv(SERVER_HOST);
        return properties.getProperty(SERVER_HOST, DEFAULT_SERVER_HOST);
    }

    @Override
    public @NotNull String getServerPort() {
        if (System.getProperties().containsKey(SERVER_PORT))
            return System.getProperty(SERVER_PORT);
        if (System.getenv().containsKey(SERVER_PORT))
            return System.getenv(SERVER_PORT);
        return properties.getProperty(SERVER_PORT, DEFAULT_SERVER_PORT);
    }

    @NotNull
    @Override
    public String getSessionSecret() {
        if (System.getProperties().containsKey(SESSION_SECRET_KEY))
            return System.getProperty(SESSION_SECRET_KEY);
        if (System.getenv().containsKey(SESSION_SECRET_KEY))
            return System.getenv(SESSION_SECRET_KEY);
        return properties.getProperty(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionIteration() {
        @Nullable final String systemProperty = System.getProperty(SESSION_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(SESSION_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT));
    }

    @NotNull
    @Override
    public Integer getBackupInterval() {
        @Nullable final String systemProperty = System.getProperty(BACKUP_INTERVAL_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(BACKUP_INTERVAL_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT));
    }

}
