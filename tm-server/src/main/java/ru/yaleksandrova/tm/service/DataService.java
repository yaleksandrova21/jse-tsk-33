package ru.yaleksandrova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.codehaus.plexus.util.Base64;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.sevice.*;
import ru.yaleksandrova.tm.component.Backup;
import ru.yaleksandrova.tm.dto.Domain;

import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.model.User;
import java.util.Base64.Encoder;
import java.util.Base64.Decoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DataService implements IDataService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public DataService(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    private void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;

        serviceLocator.getProjectService().clear();
        @Nullable final List<Project> projects = domain.getProjects();
        if (!projects.isEmpty()) serviceLocator.getProjectService().addAll(projects);

        serviceLocator.getTaskService().clear();
        @Nullable final List<Task> tasks = domain.getTasks();
        if (!tasks.isEmpty()) serviceLocator.getTaskService().addAll(tasks);

        serviceLocator.getUserService().clear();
        @Nullable final List<User> users = domain.getUsers();
        if (!users.isEmpty()) serviceLocator.getUserService().addAll(users);

        serviceLocator.getSessionService().clear();
    }

    private void setSystemProp() {
        @NotNull final String jaxbSystemPropKey = serviceLocator.getPropertyService().getJaxbSystemPropKey();
        @NotNull final String jaxbSystemPropValue = serviceLocator.getPropertyService().getJaxbSystemPropValue();
        System.setProperty(jaxbSystemPropKey, jaxbSystemPropValue);
    }

    @Override
    @SneakyThrows
    public void saveBackup() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final String fileName = serviceLocator.getPropertyService().getFileBackupName();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadBackup() {
        @NotNull final String fileName = serviceLocator.getPropertyService().getFileBackupName();
        @NotNull final File file = new File(fileName);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(fileName)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @Nullable final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveBinaryData() {
        @NotNull final Domain domain = getDomain();
        @NotNull final String fileName = serviceLocator.getPropertyService().getFileBinaryName();
        @NotNull final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadBinaryData() {
        @NotNull final String fileName = serviceLocator.getPropertyService().getFileBinaryName();
        @NotNull final File file = new File(fileName);
        if (!file.exists()) throw new NoFileException(fileName);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(fileName);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveBase64Data() {
        @NotNull final Domain domain = getDomain();
        @NotNull final String fileName = serviceLocator.getPropertyService().getFileBase64Name();
        @NotNull final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new Base64().encode(bytes).toString();

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadBase64Data() {
        @NotNull final String fileName = serviceLocator.getPropertyService().getFileBase64Name();
        @NotNull final File file = new File(fileName);
        if (!file.exists()) throw new NoFileException(fileName);
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(fileName)));
        @NotNull final byte[] decodedData = new Base64().decode(base64.getBytes());
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveFasterXmlData(final boolean isJson) {
        @NotNull String fileName;
        if (isJson)
            fileName = serviceLocator.getPropertyService().getFileFasterXmlJsonName();
        else
            fileName = serviceLocator.getPropertyService().getFileFasterXmlXmlName();
        @NotNull final Domain domain = getDomain();
        @NotNull ObjectMapper objectMapper;
        if (isJson)
            objectMapper = new ObjectMapper();
        else
            objectMapper = new XmlMapper();
        @NotNull final String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        fileOutputStream.write(data.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadFasterXmlData(final boolean isJson) {
        @NotNull String fileName;
        if (isJson)
            fileName = serviceLocator.getPropertyService().getFileFasterXmlJsonName();
        else
            fileName = serviceLocator.getPropertyService().getFileFasterXmlXmlName();
        @NotNull final File file = new File(fileName);
        if (!file.exists()) throw new NoFileException(fileName);
        @NotNull final String data = new String(Files.readAllBytes(Paths.get(fileName)));
        @NotNull ObjectMapper objectMapper;
        if (isJson)
            objectMapper = new ObjectMapper();
        else
            objectMapper = new XmlMapper();
        @Nullable final Domain domain = objectMapper.readValue(data, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveJaxbData(final boolean isJson) {
        if (isJson) setSystemProp();
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        if (isJson) {
            @NotNull final String jaxbMediaType = serviceLocator.getPropertyService().getJaxbMediaType();
            jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, jaxbMediaType);
        }
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        @NotNull String fileName;
        if (isJson)
            fileName = serviceLocator.getPropertyService().getFileJaxbJsonName();
        else
            fileName = serviceLocator.getPropertyService().getFileJaxbXmlName();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadJaxbData(final boolean isJson) {
        @NotNull String fileName;
        if (isJson)
            fileName = serviceLocator.getPropertyService().getFileJaxbJsonName();
        else
            fileName = serviceLocator.getPropertyService().getFileJaxbXmlName();
        @NotNull final File file = new File(fileName);
        if (!file.exists()) throw new NoFileException(fileName);
        if (isJson) setSystemProp();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        if (isJson) {
            @NotNull final String jaxbMediaType = serviceLocator.getPropertyService().getJaxbMediaType();
            jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, jaxbMediaType);
        }
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);
        setDomain(domain);
    }

}
