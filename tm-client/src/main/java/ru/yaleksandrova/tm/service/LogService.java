package ru.yaleksandrova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.sevice.ILogService;
import java.io.InputStream;
import java.io.IOException;
import java.util.logging.*;

public final class LogService implements ILogService {

    @NotNull
    private final static String COMMANDS = "COMMANDS";
    @NotNull
    private final static String COMMANDS_FILE = "./log/commands.txt";

    @NotNull
    private final static String MESSAGES = "MESSAGES";
    @NotNull
    private final static String MESSAGES_FILE = "./log/messages.txt";

    @NotNull
    private final static String ERRORS = "ERRORS";
    @NotNull
    private final static String ERRORS_FILE = "./log/errors.txt";

    @NotNull
    private final ConsoleHandler consoleHandler = new ConsoleHandler();
    @NotNull
    private final LogManager manager = LogManager.getLogManager();
    @NotNull
    private final Logger root = Logger.getLogger("");
    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);
    @NotNull
    private final Logger messages = Logger.getLogger(MESSAGES);
    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);

    {
        init();
        registry(errors, ERRORS_FILE, true);
        registry(commands, COMMANDS_FILE, false);
        registry(messages, MESSAGES_FILE, true);
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            @NotNull final InputStream inputStream = LogService.class.getResourceAsStream("/logger.properties");
            manager.readConfiguration(inputStream);
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(@NotNull final Logger logger, @NotNull final String filename, @NotNull final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void debug(@Nullable String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void command(@Nullable String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(@Nullable Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
