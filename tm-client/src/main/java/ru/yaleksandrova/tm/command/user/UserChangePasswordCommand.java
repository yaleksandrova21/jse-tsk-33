package ru.yaleksandrova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class UserChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change user password";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD :");
        @NotNull final String password = ApplicationUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}
