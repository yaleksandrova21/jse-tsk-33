package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.api.repository.IOwnerRepository;
import ru.yaleksandrova.tm.model.AbstractOwnerEntity;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {
}

